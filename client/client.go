/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package client

import (
	"fmt"
	"gitlab.com/kubernetes/context"

	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth" // Initialize all known client auth plugins
)

var (
	Client        = getClientset
	DynamicClient = getDynamicClient
	DefaultClient = getDefaultClientset
)

// getClientset 根据context上下文创建k8s客户端.
// clientSet与dynamicClient区别：
// clientSet：能操作内置资源
// dynamicClient: 能操作内置资源和自定义资源
func getClientset(kubeContext string) (kubernetes.Interface, error) {
	config, err := context.GetRestClientConfig(kubeContext)
	if err != nil {
		return nil, fmt.Errorf("getting client config for Kubernetes client: %w", err)
	}
	return kubernetes.NewForConfig(config)
}

// getDynamicClient 根据创建文context创建k8s客户端.
func getDynamicClient(kubeContext string) (dynamic.Interface, error) {
	config, err := context.GetRestClientConfig(kubeContext)
	if err != nil {
		return nil, fmt.Errorf("getting client config for dynamic client: %w", err)
	}
	return dynamic.NewForConfig(config)
}

// getDefaultClientset 根据默认的context上下文创建k8s客户端.
func getDefaultClientset() (kubernetes.Interface, error) {
	config, err := context.GetDefaultRestClientConfig()
	if err != nil {
		return nil, fmt.Errorf("getting client config for Kubernetes client: %w", err)
	}
	return kubernetes.NewForConfig(config)
}
